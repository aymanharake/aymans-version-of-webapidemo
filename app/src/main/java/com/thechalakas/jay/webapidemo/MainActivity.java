package com.thechalakas.jay.webapidemo;

/*
 * Created by jay on 13/09/17. 11:35 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

//references used
//http://www.techrepublic.com/blog/software-engineer/calling-restful-services-from-your-android-app/
//http://www.androidauthority.com/use-remote-web-api-within-android-app-617869/

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
{

    //get the button handle

    Button button_call_api_1;

    //get the textview_api_results text handle

    TextView textview_api_results;

    //get the progress bar handle
    //Lets get the progress bar shall we?
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the button handle

        button_call_api_1 = (Button) findViewById(R.id.button_call_api_1);

        //get the textview_api_results text handle

        textview_api_results = (TextView) findViewById(R.id.textview_api_results);
        //use this to make the text scrollable.
        //also on the xml page put this
        // android:scrollbars="vertical"
        textview_api_results.setMovementMethod(new ScrollingMovementMethod());

        //get the progress bar handle
        //Lets get the progress bar shall we?
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //make progress bar invisible
        progressBar.setVisibility(View.GONE);

        button_call_api_1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String msg = "button_call_api_1 setOnClickListener reached";
                Log.i("Button Clicks",msg);
                //variable used to show the text on the app
                //String textview_api_results_msg = "json stuff should come here";

                //alright, lets call the API
                ConsumeAPI2 consumeapi2 = new ConsumeAPI2();
                consumeapi2.execute();

            }
        });
    }


    //class that will be used to make the web api call
    class ConsumeAPI2 extends AsyncTask<Object, Object, String> {


        //make the progress bar appear, to indicate that we have begun the web call
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        //do the actual web call and return the results
        @Override
        protected String doInBackground(Object... urls) {
            try {
                //your HTTP API address here. you can put any URL with any combination (like api KEY, php, and so on)
                //this is an API that I built for testing purposes which returns a collection of headphones.
                String API_URL = "http://192.168.1.2/Android/includes/getData.php";
                //a string wont work on android directly even if it looks like a URL you need to use a URL object
                URL url = new URL(API_URL);

                //following code is boiler plate. remains same almost everytime
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                try {

                    Map<String, String> params = new LinkedHashMap<>();
                    params.put("question", "q2"); // All parameters, also easy

                    StringBuilder postData = new StringBuilder();
                    // POST as urlencoded is basically key-value pairs, as with GET
                    // This creates key=value&key=value&... pairs
                    for (Map.Entry<String, String> param : params.entrySet()) {
                        if (postData.length() != 0) postData.append('&');
                        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                        postData.append('=');
                        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    }

                    // Convert string to byte array, as it should be sent
                    byte[] postDataBytes = postData.toString().getBytes("UTF-8");


                    // Tell server that this is POST and in which format is the data
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                    conn.setDoOutput(true);
                    Log.i("MainActivity", "testing1: ");


                    conn.getOutputStream().write(postDataBytes);
                    Log.i("MainActivity", "testing2: ");

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    String inputLine;
                    ArrayList<String> myArray = new ArrayList<>();
                    while ((inputLine = in.readLine()) != null)
                        myArray.add(inputLine);
                    Log.i("MainActivity", "inputLine:"+ inputLine);
                    in.close();
                    Log.i("MainActivity", "exception: ");
                    Log.i("MainActivity", "that didnt work: ");
                    return inputLine;



                }
                //when the network call is over disconnect from the URL connection
                finally {
                    conn.disconnect();
                }
            }
            //if something went wrong, catch the exception and show a simple error
            catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }


        //make the progress bar go away. the web call is completed.
        protected void onPostExecute(String response)
        {
            progressBar.setVisibility(View.GONE);

            //do something with the response
            String msg = response;
            textview_api_results.setText(msg);
        }
    }
}
